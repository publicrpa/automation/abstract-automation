package publicrpa.automation.email;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import publicrpa.automation.Configuration;

public class SendgridEmailChannel {

	final private String sendgridAPIKey;
	final private String sendgridEmailFrom;
	final private List<String> emailsTo;

	public SendgridEmailChannel(Configuration config) {
		this.sendgridAPIKey = config.get("sendgridAPIKey");
		this.sendgridEmailFrom = config.get("sendgridEmailFrom");
		this.emailsTo = Arrays.asList(config.get("sendEmailTo").split(";"));
	}

	public void sendEmails(String subject, String body) {
		this.sendEmails(subject, body, Arrays.asList());
	}

	public void sendEmails(String subject, String body, List<Path> files) {
		emailsTo.forEach(email -> sendEmailWithAttachment(email, subject, body, files));
	}

	private void sendEmailWithAttachment(String emailTo, String subject, String body, List<Path> files) {
		Email from = new Email(sendgridEmailFrom);
	    Email to = new Email(emailTo);
		Content content = new Content("text/html", body);
	    Mail mail = new Mail(from, subject, to, content);
	    files.stream().map(p -> createAttachement(p)).forEach(att -> mail.addAttachments(att));
	    SendGrid sg = new SendGrid(sendgridAPIKey);
	    Request request = new Request();
		request.setMethod(Method.POST);
		request.setEndpoint("mail/send");
		try {
			request.setBody(mail.build());
			sg.api(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendEmailWithException(Exception exception, List<Path> screenshots) {
		Email from = new Email(sendgridEmailFrom);
	    Email to = new Email(emailsTo.get(0));
	    final var bodyText = "Excpetion: "
	    						+ exception.getLocalizedMessage() + "<br><br>"
	    						+ Arrays.asList(exception.getStackTrace()).stream().map(ste -> ste.toString()).collect(Collectors.joining("<br>\n"));
		Content content = new Content("text/html", bodyText);
	    Mail mail = new Mail(from, "Wyjątek w trakcie przetwarzania", to, content);
	    screenshots.stream().map(scr -> createAttachement(scr)).forEach(att -> mail.addAttachments(att));
	    SendGrid sg = new SendGrid(sendgridAPIKey);
	    Request request = new Request();
		request.setMethod(Method.POST);
		request.setEndpoint("mail/send");
		try {
			request.setBody(mail.build());
			sg.api(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Attachments createAttachement(Path scr) {
		final var result = new Attachments();
		result.setFilename(scr.getFileName().toString());
		result.setType("image/x-png");
		result.setDisposition("attachment");
		try {
			result.setContent(Base64.getEncoder().encodeToString(Files.readAllBytes(scr)));
		} catch (IOException e) {
			e.printStackTrace();
		}
        return result;
	}

	public void sendEmailTo(String emailTo, String subject, String body) {
		sendEmailWithAttachment(emailTo, subject, body, new ArrayList<>());
	}
	
}
