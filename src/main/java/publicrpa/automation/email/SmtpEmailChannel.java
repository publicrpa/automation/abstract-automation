package publicrpa.automation.email;

import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import publicrpa.automation.Configuration;

public class SmtpEmailChannel {

	final private String login;
	final private String name;
	final private String password;
	final private List<String> emailsTo;
	final private String startTLS;
	final private String port;
	final private String host;
	final private String auth;

	public SmtpEmailChannel(Configuration config) {
		this.login = config.get("smtpLogin");
		this.name = config.get("smtpName");
		this.password = config.get("smtpPassword");
		this.startTLS = config.get("smtpStartTLS");
		this.port = config.get("smtpPort");
		this.host = config.get("smtpHost");
		this.auth = config.get("smtpAuth");
		this.emailsTo = Arrays.asList(config.get("sendEmailTo").split(";"));
		if (login == null ||
			name == null ||
			password == null ||
			startTLS == null ||
			port == null ||
			host == null ||
			auth == null ||
			emailsTo.isEmpty()
				) {
			throw new IllegalArgumentException("brak parametrów smtp!!! nie mogę kontynuować.");
		}
	}

	public void sendEmails(String subject, String body) {
		this.sendEmails(subject, body, Arrays.asList());
	}

	public void sendEmails(String subject, String body, List<Path> files) {
		emailsTo.forEach(email -> sendEmailWithAttachment(email, subject, body, files));
	}

	public void sendEmailWithAttachment(String emailTo, String subject, String body, List<Path> files) {

		Authenticator authenticator = new Authenticator() {
	        @Override
	        protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(login, password);
	        }       
		};

		Properties props = System.getProperties();
	    props.put("mail.smtp.starttls.enable", startTLS);
	    props.put("mail.smtp.port", port);
	    props.put("mail.smtp.host", host);
	    props.put("mail.smtp.auth", auth);
		Session session = Session.getInstance(props, authenticator);
		MimeMessage msg = new MimeMessage(session);
		try {
			msg.setFrom(new InternetAddress(login, name));
			msg.addRecipient(RecipientType.TO, new InternetAddress(emailTo));
			msg.setSubject(subject);
//			msg.setText(body, "utf-8", "html");

	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setText(body, "utf-8", "html");

	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);

	        files.forEach(p -> {
	        	try {
	        		BodyPart attachementPart = new MimeBodyPart();
					attachementPart.setDataHandler(new DataHandler(new FileDataSource(p.toFile())));
					attachementPart.setFileName(p.getFileName().toString());
					multipart.addBodyPart(attachementPart);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
	        });

	        msg.setContent(multipart);

			Transport.send(msg);
		} catch (MessagingException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	public void sendEmailWithException(Exception exception, List<Path> screenshots) {
	    final var bodyText = "Excpetion: "
	    						+ exception.getLocalizedMessage() + "<br><br>"
	    						+ Arrays.asList(exception.getStackTrace()).stream().map(ste -> ste.toString()).collect(Collectors.joining("<br>\n"));
		sendEmailWithAttachment(emailsTo.get(0), "Wyjątek w trakcie przetwarzania", bodyText, screenshots);
	}
	
}
