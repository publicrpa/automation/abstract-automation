package publicrpa.automation.monitoring;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import io.github.classgraph.ClassGraph;
import publicrpa.automation.Configuration;

public class MonitoringRegistry {

	private static final List<Monitoring> implementationList = new ArrayList<>();

	public static void register(Configuration config) {
		final var scan = new ClassGraph().enableClassInfo().scan();
		scan.getSubclasses(Monitoring.class).loadClasses().forEach(clazz -> {
			try {
				final var constructor = clazz.getConstructor(Configuration.class);
				final var instance = constructor.newInstance(config);
				implementationList.add((Monitoring)instance);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	public static void start() {
		implementationList.forEach(Monitoring::start);
	}

	public static void finish() {
		implementationList.forEach(Monitoring::finish);
	}

}
