package publicrpa.automation.monitoring;

import publicrpa.automation.Configuration;

public abstract class Monitoring {

	public Monitoring(Configuration config) {}

	public abstract void start();
	public abstract void finish();

}
