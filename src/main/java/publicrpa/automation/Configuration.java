package publicrpa.automation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

public class Configuration {

    private final Map<String, Object> configMap;

    private Configuration(final Map<String, Object> configMap) {
        this.configMap = configMap;
    }

    public static Configuration readFrom(final String jsonString) {
    	Objects.requireNonNull(jsonString, "Cannot parse null as a JSON string");
        final HashMap<String, Object> configMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<String, Object>>() {}.getType());
        return new Configuration(configMap);
    }

    public String get(final String key) {
    	return Optional.ofNullable((String) configMap.get(key)).orElseThrow(() -> new IllegalArgumentException("There is no mapping for key: " + key +". Please check the configuration file and add value for this key."));
    }

    public List<String> getList(final String key) {
        return (List<String>) configMap.get(key);
    }

    public List<LinkedTreeMap<String, Object>> getObjectList(final String key) {
        return (List<LinkedTreeMap<String, Object>>) configMap.get(key);
    }
}
