package publicrpa.automation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

import publicrpa.automation.monitoring.MonitoringRegistry;

public abstract class AbstractAutomation implements Runnable {

	protected Configuration config;

	public AbstractAutomation(String jsonString) {
		this.config = Configuration.readFrom(jsonString);
		MonitoringRegistry.register(this.config);
	}

	public void run() {
        try {
        	monitoringStart();
            beforeFlow();
            taskSpecificFlow();
            monitoringFinish();
        } catch (final Exception e) {
        	handleExpection(e);
        } finally {
            finalizeFlow();
        }
	}

	protected void monitoringFinish() {
		MonitoringRegistry.finish();
	}

	protected void monitoringStart() {
		MonitoringRegistry.start();
	}

	protected abstract void finalizeFlow();

	protected abstract void handleExpection(final Exception e);

	protected abstract void taskSpecificFlow();

	protected abstract void beforeFlow();


	protected static void log(Throwable e) {
		e.printStackTrace();
		logToFile(e.toString());
	}

	protected static void log(String text) {
		System.out.println(text);
		logToFile(text);
	}

	private static void logToFile(String text) {
		try {
			Files.write(Paths.get("output.log"), (LocalDateTime.now() + " " + text + System.lineSeparator()).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

}
